## Info

Use production build to get rid of additional rendering.

Steps to run production build using Git Bash:
```
npm run build
npx serve -p 3001 build
```

View in the browser on local port 3001.
